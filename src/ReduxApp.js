import { LitElement, html, css } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from './store/store';
import { addTodo, updateTodoStatus, updateFilter } from "./actions/actions";
import '@polymer/paper-input/paper-input.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox';
import '@vaadin/vaadin-list-box/vaadin-list-box';
import '@vaadin/vaadin-item/vaadin-item';
import '@vaadin/vaadin-select/vaadin-select';



export class ReduxApp extends connect(store)(LitElement) {
  static get properties() {
    return {
      todos: Array
    };
  }

  static get styles() {
    return css`
    `;
  }

  async stateChanged(state) {
    this.todos = state.todos;
    this.filter = state.filter;
    await this.requestUpdate();

  }

  constructor() {
    super();
    this.todos = [];
  }



  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);

    this.shadowRoot.querySelector('#filter').renderer = root => {
      if(root.firstElementChild) {
        return;
      }

      root.innerHTML = `
        <vaadin-list-box>
            <vaadin-item value="All">Todos</vaadin-item>
            <vaadin-item value="Active">Pendientes</vaadin-item>
            <vaadin-item value="complete">Completados</vaadin-item>
        </vaadin-list-box>
      `;
    }

  }


  render() {
    return html`
        <h2>Filtro selecionado ${this.filter}</h2>
        <paper-input label="Add new task"></paper-input>
        <button @click="${this.addTodo}">ADD</button>

        <vaadin-select id="filter" @value-changed="${this.filterChanged}"></vaadin-select>

        ${this.todos.map(todo => html`
          <vaadin-checkbox .todo="${todo}" ?checked="${todo.complete}" @change="${this.updateTodosStatus}">
          <!--vaadin-checkbox .todo="${todo}" ?checked="${todo.complete}" @change="${e => this.updateTodosStatus(todo, e.target.checked)}"-->
            ${todo.task}
          </vaadin-checkbox>
        `)}
    `;
  }


  addTodo() {
    const task = this.shadowRoot.querySelector('paper-input');
    if(task.value !== '') {
      store.dispatch(addTodo(task.value));
      task.value = '';
    }
  }

  filterChanged(event) {
    if(event.detail.value && event.detail.value !== '') {
        store.dispatch(updateFilter(this.todos, event.detail.value));
    }
  }

  updateTodosStatus(event) {
    const todo = event.currentTarget.getAttribute('todo');
    //console.table(event.detail, event.target.checked, event.target.todo);
    store.dispatch(updateTodoStatus(event.target.todo,event.target.checked))
    //store.dispatch(updateTodoStatus(todo, checked))
    //store.dispatch(updateTodoStatus(todo, event.target.checked))
  }


}
