// importar actions
import { ADD_TODO, UPDATE_TODO_STATUS, UPDATE_FILTER } from '../actions/actions';

export const VisibilityFilters = {
  SHOW_ALL: 'All',
  SHOW_ACTIVE: 'Active',
  SHOW_COMPLETE: 'complete'
}

const initialState = {
  todos: [],
  filter: VisibilityFilters.SHOW_ALL
}


export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return { ...state, todos:[...state.todos, action.todo] }

    case UPDATE_TODO_STATUS:
      return {
        ...state,
        todos: state.todos.map(todo => todo.task === action.todo.task ? {...action.todo, completed: action.complete}: todo)
      }

    case UPDATE_FILTER:
      let todos = [];
      todos = state.todos;
      return {
        ...state,
        todos: todos.filter(todo => {
          if(action.filter === 'complete'){
            return todo.completed == true;
          }else if(action.filter === 'All'){
            return todo.complete == false;
          }else if(action.filter === 'Active'){
            return todo.completed == false || todo.completed == undefined;
          }else{
            return todo;
          }
        })
      }

    default:
      return state
  }
}
